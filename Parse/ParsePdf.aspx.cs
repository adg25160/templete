﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using org.apache.pdfbox.pdmodel;
using org.apache.pdfbox.util;

namespace Parse
{
    public partial class ParsePdf : System.Web.UI.Page
    {
        public static string res;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnParse_Click(object sender, EventArgs e)
        {
            //儲存位置
            String savePath = @"C:\upload";
            if (FileUpload1.HasFile)
            {
                String fileName = FileUpload1.FileName;
                savePath += fileName;
                FileUpload1.SaveAs(savePath);
                Label1.Text = "執行結果：成功";
            }
            else
            {
                Label1.Text = "執行結果：失敗";
            }
            PDDocument doc = PDDocument.load(savePath);
            //Stripper是PDF轉換過的文字
            PDFTextStripper Stripper = new PDFTextStripper();
            TextBox1.Text = (Stripper.getText(doc));
            //res = Stripper.getText(doc);
        }
    }
}