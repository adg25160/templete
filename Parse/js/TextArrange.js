/*!
 * TextArrange v0.0.2
 * Powered by SnycKure
 * Copyright 2018
 */

//補上SwitchID，若遭遇到BUG，可關閉此功能
function TextArrange(TextAreaID,SwitchID){
	if(!$('#'+SwitchID).prop('checked')){return;}
	let text = document.getElementById(TextAreaID).value;
	text = SpaceSelecting(text);
	text = CommonChanging(text);
	text = ColonChanging(text);
	text = SemiColonChanging(text);
	text = PeriodinTextChanging(text);
	text = CheckPeriod(text);
	document.getElementById(TextAreaID).value = text;
}

function SpaceSelecting(text){
	var SearchE = text.replace(/\n/g," ").replace(/\t/g," ").replace(/\u3000/g," ");
	let temp = "";
	while(true){
		let Pos = SearchE.search(" ");
		if (Pos < 0) break;
		let NEndofText = SearchE.length;
		temp += SearchE.slice(0,Pos);
		var OffPos = Pos ;
		while (SearchE.slice(OffPos+1,OffPos+2) == " "){
			OffPos += 1 ;
		}
		if(( IsAlphabet(SearchE.slice(Pos-1,Pos)) || IsNumber(SearchE.slice(Pos-1,Pos)) ) && ( IsAlphabet(SearchE.slice(OffPos+1,OffPos+2)) || IsNumber(SearchE.slice(OffPos+1,OffPos+2)) )){
			temp += " ";
		}
		SearchE = SearchE.slice(OffPos+1,NEndofText+1);
	}
	temp += SearchE;
	return temp;
}

function CommonChanging(text){
	let temp = "";
	while(true){
		let Pos = text.search(",");
		if (Pos < 0) break;
		let NEndofText = text.length;
		temp += text.slice(0,Pos);
		if(( IsAlphabet(text.slice(Pos-1,Pos)) || IsNumber(text.slice(Pos-1,Pos)) ) && ( IsAlphabet(text.slice(Pos+1,Pos+2)) || IsNumber(text.slice(Pos+1,Pos+2)) )){
			temp += ", ";
		}
		else{
			temp += "\uff0c";
		}
		text = text.slice(Pos+1,NEndofText+1);
	}
	temp += text;
	return temp;
}

function ColonChanging(text){
	let temp = "";
	while(true){
		let Pos = text.search(":");
		if (Pos < 0) break;
		let NEndofText = text.length;
		temp += text.slice(0,Pos);
		if(( IsAlphabet(text.slice(Pos-1,Pos)) || IsNumber(text.slice(Pos-1,Pos)) ) && ( IsAlphabet(text.slice(Pos+1,Pos+2)) || IsNumber(text.slice(Pos+1,Pos+2)) )){
			temp += ": ";
		}
		else{
			temp += "\uff1a";
		}
		text = text.slice(Pos+1,NEndofText+1);
	}
	temp += text;
	return temp;
}

function SemiColonChanging(text){
	let temp = "";
	while(true){
		let Pos = text.search(";");
		if (Pos < 0) break;
		let NEndofText = text.length;
		temp += text.slice(0,Pos);
		if(( IsAlphabet(text.slice(Pos-1,Pos)) || IsNumber(text.slice(Pos-1,Pos)) ) && ( IsAlphabet(text.slice(Pos+1,Pos+2)) || IsNumber(text.slice(Pos+1,Pos+2)) )){
			temp += "; ";
		}
		else{
			temp += "\uff1b";
		}
		text = text.slice(Pos+1,NEndofText+1);
	}
	temp += text;
	return temp;
}

function PeriodinTextChanging(text){
	let temp = "";
	while(true){
		let Pos = text.search('\\.');
		if (Pos < 0) break;
		let NEndofText = text.length;
		temp += text.slice(0,Pos);
		if((
		( IsAlphabet(text.slice(Pos-1,Pos)) || IsNumber(text.slice(Pos-1,Pos)) ) &&
		( IsAlphabet(text.slice(Pos+1,Pos+2)) || IsNumber(text.slice(Pos+1,Pos+2)) )) && 
		!( IsNumber(text.slice(Pos-1,Pos)) && IsNumber(text.slice(Pos+1,Pos+2)) )){
			temp += ". ";
		}else{
			temp += ".";
		}
		text = text.slice(Pos+1,NEndofText+1);
	}
	temp += text;
	return temp;
}

function CheckPeriod(text){
	let NEndofText = text.length;
	let LastWord = text.slice(NEndofText-1,NEndofText+1);
	if( NEndofText > 0 ){
		if ( LastWord == '\.'){
			return text;
		}
		if ( LastWord.search("\u3002") < 0 ){
		text += "\u3002";
		}
	}
	return text;
}

function IsAlphabet(Input){
	let temp = Input.charCodeAt();
	if((temp >= 65 && temp <= 90) || (temp >= 97 && temp <= 122))
		return true;
	else
		return false;
}

function IsNumber(Input){
	let temp = Input.charCodeAt();
	if(temp >= 48 && temp <= 57)
		return true;
	else
		return false;
}