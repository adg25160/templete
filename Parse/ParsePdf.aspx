﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ParsePdf.aspx.cs" Inherits="Parse.ParsePdf" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>test</title>
    <link rel="stylesheet" href="./css/w3.css" />
    <link rel="stylesheet" href="./css/bootstrap.min.css" />
    <link rel="stylesheet" href="./css/PatientCaseDataBase.css" />
    <link rel="stylesheet" href="./css/bootstrap-toggle.min.css" />
    <link rel="stylesheet" href="./css/templete.css" />
</head>
<script type="text/javascript" src="./js/TextArrange.js"></script>
<script type="text/javascript" src="./js/jquery.min.js"></script>
<script type="text/javascript" src="./js/bootstrap.min.js"></script>
<script type="text/javascript" src="./js/bootstrap-toggle.min.js"></script>
<script type="text/javascript" src="./js/Syndromes_Analysis.js"></script>
<body>
    <form id="form1" runat="server">

        <!-- 頁首 -->
        <div id="header">
            <h1 style="padding-top: 30px;">中醫病例報告文件解析</h1>
        </div>
        <!-- 頁面 -->
        <div id="frame">
            <!-- 第一列 -->
            <div id="row">
                <div class="input-group">
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                    <br />
                    <asp:Button ID="BtnParse" runat="server" Text="開始解析" />
                    <br/>
                    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                    <br/>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </div>
                <br />
                <div class="input-group">
                    <span class="input-group-addon">◆篇名</span>
                    <asp:TextBox ID="ITitle" runat="server"></asp:TextBox>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">◆作者</span>
                    <asp:TextBox ID="IAuthor" runat="server"></asp:TextBox>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">◆年代</span>
                    <asp:TextBox ID="IAges" runat="server"></asp:TextBox>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">◆出處</span>
                    <asp:TextBox ID="IPublish" runat="server"></asp:TextBox>
                </div>
                <br />
                <font size="5">
                <b>病例基本資料：</b>
            </font>
                <div class="input-group">
                    <span class="input-group-addon">性別</span>
                    <asp:TextBox ID="IGender" runat="server"></asp:TextBox>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">年齡</span>
                    <asp:TextBox ID="IAge" runat="server"></asp:TextBox>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">身高</span>
                    <asp:TextBox ID="IHeight" runat="server"></asp:TextBox>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">體重</span>
                    <asp:TextBox ID="IWeight" runat="server"></asp:TextBox>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">婚姻狀態</span>
                    <asp:TextBox ID="IMarriage" runat="server" Width="150px"></asp:TextBox>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">居住地</span>
                    <asp:TextBox ID="IPOResidence" runat="server" Width="164px"></asp:TextBox>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">職業</span>
                    <asp:TextBox ID="ICareer" runat="server"></asp:TextBox>
                </div>
                <br />
                <b>主訴：</b>
                <textarea id="IChiefComplaint" style="display: block; height: 186px; width: 100%; overflow-y: scroll" onchange="TextArrange('IChiefComplaint','ISwitchBtn');"></textarea>
            </div>
            <!-- 第二列 -->
            <div id="row">
                <h2>病例病史：</h2>
                <div class="col-lg-6 col-md-6">
                    <h3>現病史：</h3>
                    <textarea id="ICurrentMedicalHistoryContent" style="display: block; height: 250px; width: 100%; overflow-y: scroll" onchange="TextArrange('ICurrentMedicalHistoryContent','ISwitchBtn');"></textarea>
                </div>
                <div class="col-lg-6 col-md-6">
                    <h3>過去病史：</h3>
                    <textarea id="IPastMedicalHistoryContent" style="display: block; height: 250px; width: 100%; overflow-y: scroll" onchange="TextArrange('IPastMedicalHistoryContent','ISwitchBtn');"></textarea>
                </div>
                <div class="col-lg-6 col-md-6">
                    <h3>個人史：</h3>
                    <textarea id="IPersonalHistoryContent" style="display: block; height: 250px; width: 100%; overflow-y: scroll" onchange="TextArrange('IPersonalHistoryContent','ISwitchBtn');"></textarea>
                </div>
                <div class="col-lg-6 col-md-6">
                    <h3>家族病史：</h3>
                    <textarea id="IFamilyHistoryContent" style="display: block; height: 250px; width: 100%; overflow-y: scroll" onchange="TextArrange('IFamilyHistoryContent','ISwitchBtn');"></textarea>
                </div>
            </div>
            <!-- 行 -->
            <div id="column">
                <h2>中醫四診：</h2>
                <h3>原文：</h3>
                <div class="col-lg-3 col-md-6">
                    <h4>望：</h4>
                    <asp:TextBox ID="IObservationDiagnosisOrigin" runat="server" TextMode="MultiLine" ReadOnly="False"></asp:TextBox>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h4>聞：</h4>
                    <asp:TextBox ID="IListenDiagnosisOrigin" runat="server" TextMode="MultiLine" ReadOnly="False"></asp:TextBox>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h4>問：</h4>
                    <asp:TextBox ID="IAskDiagnosisOrigin" runat="server" TextMode="MultiLine" ReadOnly="False"></asp:TextBox>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h4>切：</h4>
                    <asp:TextBox ID="IPulseDiagnosisOrigin" runat="server" TextMode="MultiLine" ReadOnly="False"></asp:TextBox>
                </div>
            </div>
            <div id="column">
                <h2>症狀字詞：</h2>
                <div class="col-lg-3 col-md-6">
                    <h4>望：</h4>
                    <asp:TextBox ID="IObservationDiagnosis" runat="server" TextMode="MultiLine" ReadOnly="False"></asp:TextBox>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h4>聞：</h4>
                    <asp:TextBox ID="IListenDiagnosis" runat="server" TextMode="MultiLine" ReadOnly="False"></asp:TextBox>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h4>問：</h4>
                    <asp:TextBox ID="IAskDiagnosis" runat="server" TextMode="MultiLine" ReadOnly="False"></asp:TextBox>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h4>切：</h4>
                    <asp:TextBox ID="IPulseDiagnosis" runat="server" TextMode="MultiLine" ReadOnly="False"></asp:TextBox>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h4>西醫診斷：</h4>
                    <asp:TextBox ID="IWesternMedicalDiagnosis" runat="server" TextMode="MultiLine" ReadOnly="False" Height="178px"></asp:TextBox>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h4>辨病：</h4>
                    <asp:TextBox ID="IDiseaseDifferentiation" runat="server" TextMode="MultiLine" ReadOnly="False"></asp:TextBox>
                    <h4>辨證：</h4>
                    <asp:TextBox ID="ISyndromeDifferentiation" runat="server" TextMode="MultiLine" ReadOnly="False"></asp:TextBox>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h4>治則：</h4>
                    <asp:TextBox ID="ITreatmentPrinciples" runat="server" TextMode="MultiLine" ReadOnly="False" Height="178px"></asp:TextBox>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h4>方藥：</h4>
                    <asp:TextBox ID="IPrescription" runat="server" TextMode="MultiLine" ReadOnly="False" Height="178px"></asp:TextBox>
                </div>
            </div>

            <!-- 頁尾 -->
            <div id="footer">
                <p style="font-size: 0.8em">Copyright (c) 2019-2022</p>
                <p style="font-size: 0.8em">GAIS Lab <span>ㄈJ徵女友</span></p>
            </div>
    </form>
</body>
</html>
